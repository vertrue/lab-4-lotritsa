#include <bits/stdc++.h>
//kek

using namespace std;

const int nmax = 300;
const int mmax = 35;
const int mod = 1e9+7;

long long fib[nmax];

char inc_symb(char num)
{
    if (num == 'F')
        return num;
    if (num == '8')
        return 'A';
    else
        return num+1;
}
char uninc_symb(char num)
{
    if (num == '0')
        return num;
    if (num == 'A')
        return '8';
    else
        return num-1;
}

string inc_color(string num)
{
    if (num[1] == 'F')
    {
        num[0] = inc_symb(num[0]);
        num[1] = '0';
    }
    else
    {
        num[1] = inc_symb(num[1]);
    }
    return num;
}

string uninc_color(string num)
{
    if (num[1] == '0')
    {
        num[0] = uninc_symb(num[0]);
        num[1] = 'F';
    }
    else
    {
        num[1] = uninc_symb(num[1]);
    }
    return num;
}

int main()
{
    freopen("test.html", "w", stdout);
    cout << "<!DOCTYPE HTML>\n";
    cout << "<html>\n";
    cout <<  "  <head>\n";
    cout <<          "    <meta charset=\"utf-8\">\n";
    cout <<  "      <title> Lab3 </title>\n";
    cout << "  </head>\n\n";
    cout <<   "  <body>\n";
    cout <<  "    <table align=\"center\" bgcolor=#AACCDD border=4 bordercolor=#55AADD cellpadding=11>\n" ;
    cout << "<tr bgcolor = #01DD77>";
    cerr << setw(3) << 'M' << setw(3) << 'L';
    cout << "        <td align=\"center\"> <b>" << "M" << "</b> </td>\n";
    cout << "        <td align=\"center\"> <b>" << "L" << "</b> </td>\n";
    string kek_R = "55";
    string font_R = "00";
    string kek_G = "EE";
    string font_G = "00";
    string kek_B = "FF";
    string font_B = "00";
    for(int i = 0; i < mmax; i++)
    {
        cerr << setw(3) << i;
        string color_kek = "#";
        string color_font = "#";
        color_kek+=kek_R;
        color_font+=font_R;
        color_kek+=kek_G;
        color_font+=font_G;
        color_kek+=kek_B;
        color_font+=font_B;
        for (int ii = 0; ii<=6; ii++)
        {
            kek_R = uninc_color(kek_R);
            kek_G = uninc_color(kek_G);
        }
        if (i<=20)
            for (int ii = 0; ii<=8; ii++)
            {
                font_R = inc_color(font_R);
                font_G = inc_color(font_G);
                font_B = inc_color(font_B);
            }
        cout << "        <td bgcolor="<<color_kek<<"> <b> <font color="<<color_font<<">"<<to_string(i)<<"</font>  </td>\n";

    }
    cout <<  "</tr> ";
    cerr << endl;

    for(int cur_m = 1; cur_m <= mmax; cur_m++)
    {
        cout << "      <tr>\n";
        long long fib_m[nmax], cnt[mmax];
        for(int i = 0; i < mmax; i++)
            cnt[i] = 0;
        fib_m[0] = 1 % cur_m;
        fib_m[1] = 1 % cur_m;
        for(int i = 2; i < nmax; i++)
            //fib_m[i] = fib[i] % cur_m;
            fib_m[i] = (fib_m[i-1] + fib_m[i-2])  % cur_m;
        for (int i = 0; i<nmax; i++)
            cerr << fib_m[i] << ' ';
        cerr << "\n";
        bool flag = false;
        int period = 1;
        while(!flag && period < nmax)
        {
            flag = true;
            for(int i = 0; i < period; i++)
                for(int j = i; j < nmax; j += period)
                    if(fib_m[i] != fib_m[j])
                        flag = false;
            period++;
        }
        cerr << "M: " << cur_m << ' ' << period << "!\n";
        if(period <= nmax/2)
        {
            cerr << setw(3) << cur_m << setw(3) << period - 1;
            cout << "        <td align=\"center\" bgcolor = #00DD77>";
            cout <<  to_string(cur_m);
            cout << "</td>\n";
            cout << "        <td align=\"center\" bgcolor = #00DD77>";
            cout <<  to_string(period - 1);
            cout << "</td>\n";
            long long max_fib = 0;
            for(int i = 0; i < period - 1; i++)
            {
                cnt[fib_m[i]]++;
                max_fib = max(max_fib,fib_m[i]);
            }

            for(int i = 0; i <= max_fib; i++)
            {
                cerr << setw(3) << cnt[i];
                //______________0123456
                string color = "#";
                string R = "BB";
                string G = "DD";
                string B = "FF";
                for (int ii = 0; ii<cnt[i]*9; ii++)
                {
                    R = uninc_color(R);
                    //G = inc_color(G);
                    B = uninc_color(B);
                }
                color+=R;
                color+=G;
                color+=B;
                cout <<  "        <td bgcolor=";
                cout <<  color;
                cout <<  ">";
                cout <<  to_string(cnt[i]);
                cout <<  "</td>\n";
//                cout << " : " << color << " | ";
            }
            cerr << endl;
        }
        else
        {
            cerr << setw(3) << cur_m << setw(3) << '-';
            for(int i = 0; i < mmax; i++)
                cerr << setw(3) << '-';
            cerr << endl;
        }
        cout <<  "      </tr>\n";
    }
    cout <<   "    </table>\n";
    cout <<  "  </body>\n";
    cout <<  "</html>";
    return 0;
}
/*
#include <bits/stdc++.h>
//kek

using namespace std;

const int nmax = 300;
const int mmax = 20;
const int mod = 1e9+7;

long long fib[nmax];

char inc_symb(char num)
{
    if (num == 'F') return num;
    if (num == '8')
        return 'A';
    else
        return num+1;
}
char uninc_symb(char num)
{
    if (num == '0') return num;
    if (num == 'A')
        return '8';
    else
        return num-1;
}

string inc_color(string num)
{
    if (num[1] == 'F')
    {
        num[0] = inc_symb(num[0]);
        num[1] = '0';
    }
    else
    {
        num[1] = inc_symb(num[1]);
    }
    return num;
}

string uninc_color(string num)
{
    if (num[1] == '0')
    {
        num[0] = uninc_symb(num[0]);
        num[1] = 'F';
    }
    else
    {
        num[1] = uninc_symb(num[1]);
    }
    return num;
}

int main()
{
    table = "";
    //for (int i = 1; i<nmax; i++) cout << fib[i] << ' ';
    cout << "\n";
    table += "<tr bgcolor = #00DD77>";
    cout << setw(3) << 'M' << setw(3) << 'L';
    table += "        <td> <b>";
    table += "M";
    table += "</b> </td>\n";
    table += "        <td> <b>";
    table += "L";
    table += "</b> </td>\n";
    string kek_R = "55";
    string font_R = "00";
    string kek_G = "EE";
    string font_G = "00";
    string kek_B = "FF";
    string font_B = "00";
    for(int i = 0; i < mmax; i++)
    {
        cout << setw(3) << i;
        string color_kek = "#";
        string color_font = "#";
        color_kek+=kek_R;
        color_font+=font_R;
        color_kek+=kek_G;
        color_font+=font_G;
        color_kek+=kek_B;
        color_font+=font_B;
        for (int ii = 0; ii<=6; ii++)
        {
            kek_R = uninc_color(kek_R);
            kek_G = uninc_color(kek_G);
        }
        if (i<=20)
            for (int ii = 0; ii<=8; ii++)
            {
                font_R = inc_color(font_R);
                font_G = inc_color(font_G);
                font_B = inc_color(font_B);
            }
        table += "        <td bgcolor=";
        table+=color_kek;
        table+= "> <b> <font color=";
        table+=color_font;
        table+= ">";
        table += to_string(i);
        table += "</font>  </td>\n";

    }
    table += "</tr> ";
    cout << endl;

    for(int cur_m = 1; cur_m <= mmax; cur_m++)
    {
        table += "      <tr>\n";
        long long fib_m[nmax], cnt[mmax];
        for(int i = 0; i < mmax; i++)
            cnt[i] = 0;
        fib_m[0] = 1 % cur_m;
        fib_m[1] = 1 % cur_m;
        for(int i = 2; i < nmax; i++)
            //fib_m[i] = fib[i] % cur_m;
            fib_m[i] = (fib_m[i-1] + fib_m[i-2])  % cur_m;
        for (int i = 0; i<nmax; i++)
            cout << fib_m[i] << ' ';
        cout << "\n";
        bool flag = false;
        int period = 1;
        while(!flag && period < nmax)
        {
            flag = true;
            for(int i = 0; i < period; i++)
                for(int j = i; j < nmax; j += period)
                    if(fib_m[i] != fib_m[j])
                        flag = false;
            period++;
        }
        cout << "M: " << cur_m << ' ' << period << "!\n";
        if(period <= nmax/2)
        {
            cout << setw(3) << cur_m << setw(3) << period - 1;
            table += "        <td bgcolor = #00DD77>";
            table += to_string(cur_m);
            table += "</td>\n";
            table += "        <td bgcolor = #00DD77>";
            table += to_string(period - 1);
            table += "</td>\n";
            long long max_fib = 0;
            for(int i = 0; i < period - 1; i++)
            {
                table += "        <td bgcolor = #00DD77>";
                table += to_string(fib_m[i]);
                table += "</td>\n";
            }

            cout << endl;
        }
        else
        {
            cout << setw(3) << cur_m << setw(3) << '-';
            for(int i = 0; i < mmax; i++)
                cout << setw(3) << '-';
            cout << endl;
        }
        table += "      </tr>\n";
    }
//kek
    string html = "<!DOCTYPE HTML>\n"
                  "<html>\n"
                  "  <head>\n"
                  "    <meta charset=\"utf-8\">\n"
                  "      <title>Lab3</title>\n"
                  "  </head>\n\n"
                  "  <body>\n"
                  "    <table bgcolor=#AACCDD border=4 bordercolor=#55AADD cellpadding=11>\n" + table +
                  "    </table>\n"
                  "  </body>\n"
                  "</html>";

    ofstream f("Lab3.html");
    f << html;
    f.close();

    return 0;
}
*/
